/*
	 Project: rinit
	     __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 07.08.2021
*/

mod parser;
mod project;
mod language_info;
use language_info::*;
use std::{collections::HashMap,
          path::PathBuf,
          env};

use anyhow::{Context, Result};

#[macro_use]
extern crate lazy_static;

// Exit codes
pub const EX_USAGE: i32 = 64; // Inncorect usage
pub const EX_NOINPUT: i32 = 66; // file not found
pub const EX_CONFIG: i32 = 78; // Config error, unconfigured or misconfigured
pub const EX_CANTCREAT: i32 = 73; // Can't create outputfile

const VERSION: &str = "0.1.0";
const MAN: &str = concat!(
    "usage: rinit [folder] [language] [-t, --template template] [-g, --git, bool] [-m, --make bool]\n",
    "                          [-p, --package package] [-f, --filename filename]\n\n",
    "       rinit [filename] [-t, --template template] [-g, --git, bool] [-m, --make bool]\n",
    "                          [-p, --package package] [-f, --filename filename]",
);
fn main() {
    let mut cli_args: Vec<String> = env::args().collect();
    let mut variables: HashMap<String, String> = HashMap::new();
    cli_args.remove(0);
    let cwd: String = std::env::current_dir().unwrap().into_os_string().into_string().unwrap();
    let (local_config_files, mut import_files, mut is_new_project): (Vec<PathBuf>, Vec<PathBuf>, bool) = find_config_files(&cwd);

    let mut args = Arguments::new();
    args.parse(cli_args);




    let (mut config, config_path, import_path) = parser::parse_global_config_file(&args.language.file_extention,
                                                                                  &mut variables,
                                                                                  &args.package,
                                                                                  &args.specified_file_name,
                                                                                  &args.project_name);

    let mut templates = Templates {
        main: String::new(),
        gitignore: String::new(),
        makefile: String::new(),
        headder: String::new(),
    };

    templates.main = get_template(&cwd,
                                  &args.language.lang_type.to_string(),
                                  &args.specified_template,
                                  &config_path).unwrap();

    templates.gitignore = parser::get_gitignore_template(&config_path,
                                                         &args.language.file_extention);


    templates.makefile = parser::get_makefile_template(&config_path, &args.language.lang_type.to_string(),
                                                       &mut args.override_makefile_flag,
                                                       &mut args.makefile_flag,args.overridable);

    let mut imports = parser::resolve_imports(&mut templates);

    parser::parse_local_config(&args.language.lang_type.to_string(),
                               local_config_files, &mut variables,
                               &mut config,
                               &mut imports);

    import_files.push(import_path);

    let imports = parser::get_imports_toml(imports,
                                           import_files,
                                           &args.language,
                                           args.headder_file);

    if args.headder_file {
        templates.headder = get_template(&cwd,
                                         &"h".to_string(),
                                         &"default".to_string(),
                                         &config_path).unwrap();
    }

    project::setup_project(&mut args, &mut is_new_project, &config, &mut variables);
    project::build_file_from_template(&mut templates.main, &imports.main, &variables).unwrap();
    project::build_file_from_template(&mut templates.gitignore, &imports.gitignore, &variables).unwrap();
    project::build_file_from_template(&mut templates.makefile, &imports.makefile, &variables).unwrap();
    project::build_file_from_template(&mut templates.headder, &imports.main, &variables).unwrap();


    project::create_project(args, templates, cwd, &is_new_project);
}

#[derive(Debug)]
pub enum Languages {
    None,
    C,
    Headder,
    Tex,
    Org,
    Go
}


#[derive(Debug)]
pub struct Language {
    lang_type: Languages,
    file_extention: String,
    start_multiline_comment: String,
    end_multiline_comment: String,
    non_comment_comment: String,
}

#[derive(Debug)]
pub struct Templates {
    main: String,
    gitignore: String,
    makefile: String,
    headder: String,
}

#[derive(Debug)]
pub struct Arguments {
    is_folder: bool,
    language: Language,
    specified_file_name: String,
    project_name: String,
    specified_template: String,
    package: String,
    gitignore: bool,
    git_flag: bool,
    override_gitflag: bool,
    makefile: bool,
    makefile_flag: bool,
    override_makefile_flag: bool,
    package_flag: bool,
    src_folder: bool,
    headder_file: bool,
    overridable: bool,
}

impl Languages {
    fn to_string(&self) -> String {
        match self {
            Languages::C => return String::from("c"),
            Languages::Headder => return String::from("h"),
            Languages::Tex => return String::from("tex"),
            Languages::Org => return String::from("org"),
            Languages::Go => return String::from("go"),
            Languages::None => return String::new(),
        }
    }
}

impl Arguments {
    fn new() -> Self {
        Arguments {
            is_folder: false,
            language: Language {
                lang_type: Languages::None,
                file_extention: String::new(),
                start_multiline_comment: String::new(),
                end_multiline_comment: String::new(),
                non_comment_comment: String::new(),
            },
            specified_file_name: String::from("main"),
            project_name: String::new(),
            specified_template: String::from("default"),
            package: String::from("main"),
            gitignore: false,
            git_flag: false,
            override_gitflag: false,
            makefile: false,
            makefile_flag: false,
            override_makefile_flag: false,
            package_flag: false,
            src_folder: false,
            headder_file: false,
            overridable: true,
        }
    }

    /// Parser cli args. Return struct of Arguments
    fn parse(&mut self, mut cli_args: Vec<String>) {
        let mut language_string = String::new();
        if cli_args.len() == 0 {
            colour::red!("Error: ");
            println!("Not enough arguments provided!");
            std::process::exit(EX_USAGE)
        }

        {
            let first_arg: String = cli_args.clone().into_iter().nth(0).unwrap();

            match first_arg.as_str() {
                "-v" | "--version" => {
                    println!("Rinit version: {}", VERSION);
                    std::process::exit(0);
                }

                "-h" | "--help" => {
                    println!("{}", MAN);
                    std::process::exit(0);
                }
                "-l" | "--list-templates" => {

                    if cli_args.len() >= 1  {
                        language_string = cli_args.remove(1);
                    }
                    else {
                        colour::red!("Error: --list-templates flag requires a language");
                        std::process::exit(EX_USAGE)

                        }

                    list_templates(std::env::current_dir().unwrap()
                                   .into_os_string()
                                   .into_string()
                                   .unwrap(),
                                   &language_string);

                    std::process::exit(0);

                    }
                _ => ()
            }

            let mut split: Vec<&str> = first_arg.split(".").collect();

            if split.len() == 1 {
                self.is_folder = true;
                self.project_name = cli_args.remove(0);
                language_string = cli_args.remove(0);
                // self.specified_file_name = first_arg;
            }
            else if split.len() == 2 {
                self.is_folder = false;
                self.project_name = split.remove(0).to_string();
                language_string = split.remove(0).to_string();
                self.specified_file_name = self.project_name.to_owned();
                cli_args.remove(0);
            }
        }

        {
            let mut arg: String;
            while cli_args.len() > 0 {
                arg = cli_args.remove(0);

                // for (i, arg) in cli_args.iter().enumerate() {
                match arg.as_str() {
                    "-t" | "--template" => self.specified_template = cli_args.remove(0).to_string(),
                    "-g" | "--git" | "--gitignore" => {
                        self.git_flag = true;
                        if cli_args.len() >= 1 && cli_args[0].chars().nth(0) != Some('-') {
                            Arguments::match_flag(
                                cli_args.remove(0),
                                &mut self.git_flag,
                                &mut self.override_gitflag,
                            );
                        }
                    }
                    "-m" | "--make" | "--makefile" => {
                        self.makefile_flag = true;
                        if cli_args.len() >= 1 && cli_args[0].chars().nth(0) != Some('-') {
                            Arguments::match_flag(
                                cli_args.remove(0),
                                &mut self.makefile_flag,
                                &mut self.override_makefile_flag,
                            );
                        }
                    }

                    "-f" | "--filename" => {
                        self.specified_file_name = cli_args.remove(0).to_string();
                    }

                    "-p" | "--package" => {
                        self.package_flag = true;
                        if cli_args.len() >= 1 {
                            self.package = cli_args.remove(0);
                        } else {
                            colour::red!("Error: ");
                            println!("Package flag was given, but no package was provided\nUsage: -p <package-name>");
                            std::process::exit(EX_USAGE);
                        }
                    }

                    "-v" | "--version" => {
                        println!("Rinit version: {}", VERSION);
                        std::process::exit(0);
                    }

                    "-h" | "--help" => {
                        println!("{}", MAN);
                        std::process::exit(0);
                    }
                    _ => {
                        println!("Unknown arg: {}", arg);
                        std::process::exit(0);
                    }

                }
            }
        }

        match language_string.as_str() {
            "tex" | "latex" => {
                self.language = Language {
                    lang_type: Languages::Tex,
                    file_extention: TEX_LANG_INFO["file_extention"].to_string(),
                    start_multiline_comment: TEX_LANG_INFO["start_comment"].to_string(),
                    end_multiline_comment: TEX_LANG_INFO["end_comment"].to_string(),
                    non_comment_comment: TEX_LANG_INFO["non_comment_comment"].to_string(),
                };

            }
            "go" | "golang" => {
                self.language = Language {
                    lang_type: Languages::Go,
                    file_extention: GO_LANG_INFO["file_extention"].to_string(),
                    start_multiline_comment: GO_LANG_INFO["start_comment"].to_string(),
                    end_multiline_comment: GO_LANG_INFO["end_comment"].to_string(),
                    non_comment_comment: GO_LANG_INFO["non_comment_comment"].to_string(),
                };

            }
            "c" => {
                self.language = Language {
                    lang_type: Languages::C,
                    file_extention: C_LANG_INFO["file_extention"].to_string(),
                    start_multiline_comment: C_LANG_INFO["start_comment"].to_string(),
                    end_multiline_comment: C_LANG_INFO["end_comment"].to_string(),
                    non_comment_comment: C_LANG_INFO["non_comment_comment"].to_string(),
                };

                self.src_folder = true;
                println!("Create headderfile? [y/N]");
                let mut ans = String::new();
                let stdin = std::io::stdin();
                stdin.read_line(&mut ans).unwrap();
                match ans.chars().nth(0) {
                    Some(c) => {
                        if c == 'y' || c == 'Y' {
                            self.headder_file = true;

                        }
                    }
                    None => ()
                }
            }
            "org" => {

                self.language = Language {
                    lang_type: Languages::Org,
                    file_extention: ORG_LANG_INFO["file_extention"].to_string(),
                    start_multiline_comment: ORG_LANG_INFO["start_comment"].to_string(),
                    end_multiline_comment: ORG_LANG_INFO["end_comment"].to_string(),
                    non_comment_comment: ORG_LANG_INFO["non_comment_comment"].to_string(),
                };

                self.specified_file_name = self.project_name.to_owned();
                self.overridable = false;
            }

            "h" => {
                if self.is_folder {
                    colour::red!("Error ");
                    println!("Creating folders based on headder file project is unsupported.");
                    std::process::exit(EX_USAGE);
                }
                self.language = Language {
                    lang_type: Languages::Headder,
                    file_extention: H_LANG_INFO["file_extention"].to_string(),
                    start_multiline_comment: H_LANG_INFO["start_comment"].to_string(),
                    end_multiline_comment: H_LANG_INFO["end_comment"].to_string(),
                    non_comment_comment: H_LANG_INFO["non_comment_comment"].to_string(),
                };

                self.specified_file_name = self.project_name.to_owned();
                self.overridable = false;
            }
            _ => {
                colour::red!("Error ");
                println!("Filetype {} not supported", language_string);
                std::process::exit(EX_USAGE);
            }
        }
    }

    fn match_flag(arg: String, flag: &mut bool, override_flag: &mut bool) {
        match arg.as_str() {
            "true" | "True" | "t" => {
                *override_flag = true;
                *flag = true;
            }
            "false" | "False" | "f" => {
                *override_flag = true;
                *flag = false
            }
            _ => {
                println!("Unknown arg! git and makefile flags only suports true/false");
                std::process::exit(EX_USAGE);
            }
        }
    }

}

impl Default for Arguments {
    fn default() -> Self {
        Self::new()
    }
}
/// returns list of paths to local config files and local import files
fn find_config_files(cwd: &String) -> (Vec<PathBuf>, Vec<PathBuf>, bool) {
    let home: String = dirs::home_dir().unwrap().into_os_string().into_string().unwrap();
    let mut dir = cwd.to_owned();
    let mut local_config_files: Vec<PathBuf> = Vec::new();
    let mut local_import_files: Vec<PathBuf> = Vec::new();
    let is_new_project: bool;

    loop {
        let config_file: String = dir.clone() + "/.rinit/rinit.toml";
        let import_file: String = dir.clone() + ".rinit/imports.toml";

        if std::path::Path::new(&config_file).exists() {
            let config_path = PathBuf::from(config_file);
            local_config_files.push(config_path);
        }
        if std::path::Path::new(&import_file).exists() {
            let import_path = PathBuf::from(import_file);
            local_import_files.push(import_path)
        }
        if home.eq(&dir) {
            break;
        }
        else {
            let mut split: Vec<&str> = dir.split("/").collect();
            split.pop();
            dir = split.join("/");
        }
    }


    if local_config_files.is_empty() {
        is_new_project = true;
    }
    else {
        is_new_project = false;
    }

    (local_config_files, local_import_files, is_new_project)
}

fn get_template(cwd: &String, language: &String, template: &String, config_path: &PathBuf) -> Result<String> {
    let home: String = dirs::home_dir().unwrap()
                                       .into_os_string()
                                       .into_string()
                                       .unwrap();
    let mut local_template_exits:bool = false;

    let mut dir = cwd.to_owned();
    let mut template_file: PathBuf;


    loop {
        template_file = PathBuf::from(dir.clone());
        template_file.push(".rinit/templates");
        template_file.push(language);
        template_file.push(template);


        if std::path::Path::new(&template_file).exists() {
            colour::green!("Info: ");
            println!("Using local template located at \"{}\"", &template_file.to_string_lossy());
            local_template_exits = true;
            break;
        }
        if home.eq(&dir) {
            break;
        }

        else {
            let mut split = dir.split("/")
                               .collect::<Vec<&str>>();
            split.pop();
            dir = split.join("/");
        }

    }

    if !local_template_exits  {
        template_file = PathBuf::from(config_path.clone());
        template_file.push("templates");
        template_file.push(language);
        template_file.push(template);
    }
    let template = std::fs::read_to_string(&template_file)
        .with_context(|| format!("Template {} was not found in {}", template, template_file.to_str().unwrap()))?;
    Ok(template)
}


fn list_templates(cwd: String, language: &String) {
    // Prints a list of available templates, based on language sting
    let home: String = dirs::home_dir().unwrap()
                                       .into_os_string()
                                       .into_string()
                                       .unwrap();

    let config_path: PathBuf = xdg::BaseDirectories::with_prefix("rinit").unwrap()
                                                                         .get_config_home();
    let mut template_path: PathBuf;
    let mut makefile_path: PathBuf;

    let mut dir = cwd.to_owned();
    let mut local_templates: Vec<String> = Vec::new();
    let mut global_templates: Vec<String> = Vec::new();
    let mut local_makefiles: Vec<String> = Vec::new();
    let mut global_makefiles: Vec<String> = Vec::new();

    loop {
        template_path = PathBuf::from(dir.clone());
        makefile_path = PathBuf::from(dir.clone());

        template_path.push(".rinit/templates");
        template_path.push(language);
        makefile_path.push(".rinit/makefiles");
        makefile_path.push(language);
        println!("{:?}", makefile_path);

        if std::path::Path::new(&template_path).exists() {
            let paths = std::fs::read_dir(template_path).unwrap();

            for path in paths {
                local_templates.push(path
                                     .unwrap()
                                     .path()
                                     .into_os_string()
                                     .into_string()
                                     .unwrap())
            }
        }
        if std::path::Path::new(&makefile_path).exists() {
            let paths = std::fs::read_dir(makefile_path).unwrap();

            for path in paths {
                local_makefiles.push(path
                                     .unwrap()
                                     .path()
                                     .into_os_string()
                                     .into_string()
                                     .unwrap())

            }
        }

        if home.eq(&dir) {
            break;
        }
        else {
            let mut split= dir.split("/")
                              .collect::<Vec<&str>>();
            split.pop();
            dir = split.join("/")
        }
    }

    let mut global_template_path = PathBuf::from(config_path.into_os_string()
                                                 .into_string()
                                                 .unwrap());
    let mut global_makefile_path = global_template_path.to_owned();

    global_template_path.push("templates");
    global_template_path.push(language);

    if std::path::Path::new(&global_template_path).exists() {
        let paths = std::fs::read_dir(global_template_path).unwrap();
        for path in paths {
            global_templates.push(path.unwrap()
                                  .path()
                                  .into_os_string()
                                  .into_string()
                                  .unwrap())
        }
    }
    global_makefile_path.push("makefiles");
    global_makefile_path.push(language);
    if std::path::Path::new(&global_makefile_path).exists() {
        let paths = std::fs::read_dir(global_makefile_path).unwrap();
        for path in paths {
            global_makefiles.push(path.unwrap()
                                  .path()
                                  .into_os_string()
                                  .into_string()
                                  .unwrap())
        }
    }

    if !global_templates.is_empty() {
        println!("Global templates: {:?}", global_templates);
    }
    if !local_templates.is_empty() {
        println!("Local templates: {:?}", local_templates);
    }
    if !global_makefiles.is_empty() {
        println!("Global makefiles: {:?}", global_makefiles)
    }
    if !local_makefiles.is_empty() {
        println!("Local makefiles: {:?}", local_makefiles)
    }

}
