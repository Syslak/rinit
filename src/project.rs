/*
Project: rinit
	     __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 07.08.2021
*/

use toml::{value::Map, Value};
use std::{collections::HashMap, io::{self, Write}, path::PathBuf};
use std::fs;

use anyhow::Result;
use fancy_regex::Regex;

use crate::{Arguments, Templates, parser::Config, EX_CANTCREAT};

pub fn create_project(parsed_args: Arguments, templates: Templates, cwd: String, is_new_project: &bool) {
    let mut project_root = PathBuf::from(cwd);
    if parsed_args.is_folder {
        project_root.push(&parsed_args.project_name);
        if project_root.exists() {
            colour::red!("Error: ");
            println!("The folder \"{}\" already exists", project_root.display());
            std::process::exit(EX_CANTCREAT);
        }
        fs::create_dir_all(&project_root).unwrap();
        println!("{:?}", is_new_project);
        if *is_new_project {
            let mut config_dir = project_root.clone();
            config_dir.push(".rinit");
            let mut config_file = config_dir.clone();
            config_file.push("rinit.toml");
            fs::create_dir(&config_dir).unwrap();
            fs::write(config_file,
                        toml::to_string(&create_local_conf_toml(&parsed_args.project_name)).expect("Could not encode TOML value"),
            ).expect("Could not write to local config file!")
        }
    }

    if parsed_args.src_folder && parsed_args.is_folder {
        let mut src_folder = project_root.to_owned();
        src_folder.push("src");
        fs::create_dir_all(&src_folder).unwrap();
        let mut main_file_name = src_folder.to_owned();
        main_file_name.push(parsed_args.specified_file_name.clone() + "." + &parsed_args.language.file_extention);
        if main_file_name.exists() {
            colour::red!("error: ");
            println!("the file \"{}\" already exists", main_file_name.display());
            std::process::exit(EX_CANTCREAT);
        }
        let mut main_file = fs::File::create(main_file_name).unwrap();
        write!(main_file, "{}", templates.main).unwrap();
    }
    else {
        let mut main_file_name = project_root.to_owned();
        main_file_name.push(parsed_args.specified_file_name.clone() + "." + &parsed_args.language.file_extention);
        if main_file_name.exists() {
            colour::red!("error: ");
            println!("the file \"{}\" already exists", main_file_name.display());
            std::process::exit(EX_CANTCREAT);
        }
        let mut main_file = fs::File::create(main_file_name).unwrap();
        write!(main_file, "{}", templates.main).unwrap();
    }

    if parsed_args.gitignore {
        let mut git_path = project_root.to_owned();
        git_path.push(".gitignore");
        if git_path.exists() {
            colour::red!("error: ");
            println!("the file \"{}\" already exists", git_path.display());
            std::process::exit(EX_CANTCREAT);
        }
        let mut git_file = fs::File::create(git_path).unwrap();
        write!(git_file, "{}", templates.gitignore).unwrap();
    }
    if parsed_args.makefile {
        let mut make_path = project_root.to_owned();
        make_path.push("Makefile");
        if make_path.exists() {
            colour::red!("error: ");
            println!("the file \"{}\" already exists", make_path.display());
            std::process::exit(EX_CANTCREAT);
        }
        let mut makefile = fs::File::create(make_path).unwrap();
        write!(makefile, "{}", templates.makefile).unwrap();
    }
    if parsed_args.headder_file {
        // TODO: FIX this
        let mut headder_file_name = project_root.to_owned();
        if parsed_args.is_folder {
            headder_file_name.push("src")
        }
        headder_file_name.push(parsed_args.project_name + ".h");
        let mut headder_file = fs::File::create(headder_file_name).unwrap();
        write!(headder_file, "{}", templates.headder).unwrap();
    }
}

fn create_local_conf_toml(project_name: &String) -> Value {
    let mut var = Map::new();
    let mut value = Map::new();
    value.insert("project".into(), Value::String(project_name.into()));
    var.insert("variables".into(), Value::Table(value));

    return Value::Table(var)
}


pub fn build_file_from_template(template: &mut String, imports: &HashMap<String, String>, variables: &HashMap<String, String>) -> Result<()>{
    {
        let mut new_template: String = String::new();

        for line in template.lines() {
            let split: Vec<&str> = line.split_whitespace().collect();
            match split.get(0).into() {
                Some(&"!import") => {
                    if let Some(key) = split.get(1) {
                        if let Some(import) = imports.get(&key.to_string()) {
                            new_template.push_str(import)
                        }
                    }
                }
                _ => {
                    new_template.push_str(line);
                    new_template.push_str("\n");
                    continue
                }
            }
        }
        *template = new_template
    }

    let re: Regex = Regex::new(r"(?<!\\)\$[a-zA-z]+\$").unwrap();


    re.captures_iter(&template.clone()).for_each(|i| {

        let key: String = i.as_ref().unwrap().get(0).unwrap().as_str().replace("$", "");
        let to_replace: &str = i.unwrap().get(0).unwrap().as_str();
        if !variables.contains_key(&key) {
            colour::yellow!("Warning: ");
            println!("Variable ${}$ was found in template, but the variable is not set in any of your config files", key);
        }
        else {
            *template = template.replace(to_replace, &variables[key.as_str()]);
        }
    });
    Ok(())
}

pub fn setup_project(parsed_args: &mut Arguments, is_new_project: &mut bool, config: &Config, variables: &mut HashMap<String, String>) {

    if !*is_new_project {
        let mut ans = String::new();
        let stdin = io::stdin();
        if parsed_args.is_folder {
            println!("New project?, [y/N]");
            stdin.read_line(&mut ans).unwrap();
            match ans.chars().nth(0) {
                Some(c) => {
                    if c == 'y' || c == 'Y' {
                        *is_new_project = true;
                        variables.insert("project".to_string(), parsed_args.project_name.to_string());
                    }
                }
                None => ()
            }
        }
    }
    parsed_args.gitignore = git_or_make(parsed_args.git_flag, parsed_args.override_gitflag, parsed_args.is_folder,
                                        &is_new_project, config.gitignore_default, config.overridable);
    parsed_args.makefile = git_or_make(parsed_args.makefile_flag, parsed_args.override_makefile_flag, parsed_args.is_folder,
                                       &is_new_project, config.makefile_default, config.overridable);

}


// TODO: This func need testing
/// decide weater or not to create gitignore or makefile
fn git_or_make(flag: bool, override_flag: bool, is_folder: bool, is_new_project: &bool, def: bool, overridable: bool) -> bool{
    if !overridable {
        return def
    }
    if flag && override_flag {
        return true
    }
    if !flag && override_flag {
        return false
    }
    if *is_new_project && flag && def && is_folder{
        return false
    }
    if *is_new_project && flag && !def && is_folder {
        return true
    }
    if *is_new_project && is_folder && def && is_folder{
        return true
    }
    if *is_new_project && is_folder && !def && is_folder {
        return false
    }
    else {
        return false
    }
}
