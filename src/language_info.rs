
/*
	 Project: rinit
	     __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 17.09.2022
*/

use std::collections::HashMap;

lazy_static! {
    pub static ref C_LANG_INFO: HashMap<&'static str, &'static str> = HashMap::from([
        ("start_comment", "/*"),
        ("end_comment", "*/"),
        ("non_comment_comment", "*"),
        ("file_extention", "c"),
    ]);
}

lazy_static! {
    pub static ref H_LANG_INFO: HashMap<&'static str, &'static str> = HashMap::from([
        ("start_comment", "/*"),
        ("end_comment", "*/"),
        ("non_comment_comment", "*"),
        ("file_extention", "c"),
    ]);
}

lazy_static! {
    pub static ref GO_LANG_INFO: HashMap<&'static str, &'static str> = HashMap::from([
        ("start_comment", "/*"),
        ("end_comment", "*/"),
        ("non_comment_comment", ""),
        ("file_extention", "go"),
    ]);
}

lazy_static! {
    pub static ref TEX_LANG_INFO: HashMap<&'static str, &'static str> = HashMap::from([
        ("start_comment", "\\begin{comment}"),
        ("end_comment", "\\end{comment}"),
        ("non_comment_comment", ""),
        ("file_extention", "tex"),
    ]);
}

lazy_static! {
    pub static ref ORG_LANG_INFO: HashMap<&'static str, &'static str> = HashMap::from([
        ("start_comment", "#+BEGIN_COMMENT"),
        ("end_comment", "#+BEGIN_COMMENT"),
        ("non_comment_comment", ""),
        ("file_extention", "org"),
    ]);
}
