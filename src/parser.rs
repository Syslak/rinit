/*
 	 Project: rinit
	     __
	 |_|(_
	 | |__)
	Author: Håvard Syslak
	Date: 07.08.2021
*/

use std::{collections::HashMap, path::PathBuf};
use toml::{map::Map, Value};
use users::{get_current_uid, get_user_by_uid};
//extern crate xdg;
use anyhow::{Context, Result};
use fancy_regex::Regex;
use crate::{EX_CONFIG, Templates, Language};


#[derive(Debug)]
pub struct Config {
    pub gitignore_default: bool,
    pub makefile_default: bool,
    pub overridable: bool,
}

#[derive(Debug)]
pub struct Imports {
    /// Contains Vec of keys to import
    pub gitignore: Vec<String>,
    pub makefile: Vec<String>,
    pub main: Vec<String>
}

#[derive(Debug)]
pub struct ImportMap {
    /// Contains map of import key and the "actual" import
    pub gitignore: HashMap<String, String>,
    pub makefile: HashMap<String, String>,
    pub main: HashMap<String, String>
}

pub fn parse_global_config_file(language: &String, variables: &mut HashMap<String, String>, package: &String, filename: &String, project: &String) -> (Config, PathBuf, PathBuf) {
    let config_path: PathBuf = xdg::BaseDirectories::with_prefix("rinit")
        .unwrap()
        .get_config_home();
    let mut config = Config {
        gitignore_default: false,
        makefile_default: false,
        overridable: true,
    };

    let mut config_file = config_path.clone();
    let mut import_file = config_path.clone();
    config_file.push("rinit.toml");
    import_file.push("imports.toml");

    if !config_file.is_file() {
        std::fs::create_dir(&config_path).expect("Could not create config dir");
        create_new_config(&config_file);
    }

    let raw_config: Value = read_toml_file(&config_file);
    // TODO: add better error handling here
    if let Some(language_section) = raw_config.get(language) {
        match language_section.get("gitignore") {
            Some(val) => {
                match val {
                    Value::Boolean(git_setting) => {
                        config.gitignore_default = *git_setting;
                    }
                    _ => {
                        colour::red!("Error: ");
                        println!("Non boolean value found for gitignore setting in global config file \"rinit.toml\" for language {}", language);
                        std::process::exit(EX_CONFIG);
                    }
                }
            }
            _ => {
                colour::red!("Error: ");
                println!("gitignore section not found in global config file \"rinit.toml\" for language {}", language);
                std::process::exit(EX_CONFIG);
            }
        }
        match raw_config[language].get("makefile") {
            Some(val) => {
                match val {
                    Value::Boolean(make_setting) => {
                        config.makefile_default = *make_setting;
                    }
                    _ => {
                        colour::red!("Error: ");
                        println!("Non boolean value found for makefile setting in global config file \"rinit.toml\" for language {}", language);
                        std::process::exit(EX_CONFIG);
                    }
                }
            }
            _ => {
                colour::red!("Error: ");
                println!("gitignore section not found in global config file \"rinit.toml\" for language {}", language);
                std::process::exit(EX_CONFIG);
            }
        }
        if let Some(val) = language_section.get("overridable") {
            match val {
                Value::Boolean(overridable) => {
                    config.overridable = *overridable;
                }
                _ => {
                    colour::red!("Error: ");
                    println!("Non boolean value found for overridable setting in global config file \"rinit.toml\" for language {}", language);
                    std::process::exit(EX_CONFIG);
                }
            }
        }
    }
    else {
        colour::red!("Error: ");
        println!("language section for {} was not found in global config file \"rinit.toml\"", language);
        std::process::exit(EX_CONFIG);
    }

    match raw_config.get("variables") {
        Some(vars) => {
            if let Value::Table(t) = vars {
                for (key, value) in t {
                    match value {
                        Value::String(v) => {
                            variables.insert(key.as_str().to_string(), v.as_str().to_string());
                        }
                        _ => {
                            colour::yellow!("Warning: ");
                            println!("Warning: Variable \"{}\" must be a string!", key);
                        }
                    }
                };
            };
        }
        None => {
            colour::red!("Error: ");
            println!("variables section not found in global config file \"rinit.toml\"");
            std::process::exit(EX_CONFIG);
        }
    }
    // Manually insert some runtime variables:
    let today =  chrono::Utc::today().format("%d.%m.%Y");
    variables.insert("DATE".to_string(), today.to_string());
    variables.insert("PACKAGE".to_string(), package.to_string());
    variables.insert("FILENAME".to_string(), filename.to_string());
    variables.insert("project".to_string(), project.to_string());
    variables.insert("HEADDER_DEF".to_string(), project.to_uppercase());
    variables.insert("HEADDER_INC".to_string(), project.to_string());


    return (config, config_path, import_file);
}

pub fn parse_local_config(language: &String, config_files: Vec<PathBuf>, variables: &mut HashMap<String, String>, config: &mut Config, imports: &mut Imports) {

    for file in config_files.iter().rev() {
        let raw_config = read_toml_file(&file);
        // raw_config.get(language).what;
        // TODO: This need better error handling
        if let Some(option) = raw_config.get(language) {
                match option.get("gitignore") {
                    Some(val) =>
                        match val {
                            Value::Boolean(git_setting) => {
                                config.gitignore_default = *git_setting;
                            }
                            _ => {
                                colour::red!("Error: ");
                                println!("Non boolean value found for gitignore setting in local config file \"{}\"", file.display());
                                std::process::exit(EX_CONFIG);
                            }
                        }
                    None => {}
                }
                match option.get("makefile") {
                    Some(val) => {
                        match val {
                            Value::Boolean(make_setting) => {
                                config.makefile_default = *make_setting;
                            }
                            _ => {
                                colour::red!("Error: ");
                                println!("Non boolean value found for makefile setting in global config file \"rinit.toml\"");
                                std::process::exit(EX_CONFIG);
                            }
                        }
                    }
                    None => {}
                };
            if let Some(val) = option.get("overridable") {
                if let Value::Boolean(overridable) = val {
                    config.overridable = *overridable;
                }
            }
        }

        if let Some(vars) = raw_config.get("variables") {
            if let Value::Table(t) = vars {
                for (key, value) in t {
                    //let key = key.replace("\"", "");
                    match value {
                        Value::String(v) => {
                            variables.insert(key.as_str().to_string(), v.as_str().to_string());
                        }
                        // TODO: Concider exiting program here.
                        _ => {
                            colour::yellow!("Warning");
                            println!("Variable \"{}\", in config file \"{}\", should be string", key, file.display())
                        }
                    }
                };
            };
        }
        else {
            colour::red!("Error: ");
            println!("variable section not found in local config file \"{}\"", file.display());
            std::process::exit(EX_CONFIG);
        }
        if let Some(override_section) = raw_config.get("override") {

            if let Some(Value::Table(lang_imports)) = override_section.get(language) {
                if let Some(toml::Value::Array(remove)) = lang_imports.get("remove") {
                    for i in remove {
                        match i {
                            toml::Value::String(v) => {
                                imports.main.retain(|x| *x != v.to_string());
                            }
                            _ => panic!("imports to remove in local config {} should be an array of strings", file.display()), // TODO: Print meir vetig panic
                        }
                    }
                }
            }
            if let Some(git_override_section) = override_section.get("gitignore") {

            if let Some(toml::Value::Array(git_imports)) = git_override_section.get("import") {
                for i in git_imports {
                    match i {
                        toml::Value::String(v) => imports.gitignore.push(v.to_string()),
                        _ => panic!("imports to remove in local config {} should be an array of strings", file.display()), // TODO: Print meir vetig panic
                    }

                }
            }
                if let Some(toml::Value::Array(git_removes)) = git_override_section.get("remove") {
                    for i in git_removes {
                        match i {
                            toml::Value::String(v) => imports.gitignore.retain(|x| *x != v.to_string()),
                            _ => panic!("imports to remove in local config {} should be an araray of strings strings", file.display()), // TODO: Print meir vetig panic
                        }
                    }
                }

            }
            sort_imports(imports);
        }
    }
}

fn sort_imports(imports: &mut Imports) {
    imports.main.sort_unstable();

    imports.gitignore.sort_unstable();
    imports.makefile.sort_unstable();
    // redundant imports
    imports.main.dedup();
    imports.gitignore.dedup();
    imports.makefile.dedup();
}
pub fn resolve_imports(templates: &mut Templates) -> Imports {
    // Returns list of imports found in template file
    let mut imports = Imports{
        gitignore: Vec::new(),
        makefile: Vec::new(),
        main: Vec::new()
    };
    imports.main = get_imports_from_template(&templates.main).unwrap();
    imports.gitignore = get_imports_from_template(&templates.gitignore).unwrap();
    imports.makefile = get_imports_from_template(&templates.makefile).unwrap();

    return imports
}

fn get_imports_from_template(template: &String) -> Result<Vec<String>> {
    let mut imports = Vec::new();
    for line in template.lines() {
        let split: Vec<&str> = line.split_whitespace().collect();
        //if let first = split.get(0).into() {
        match split.get(0).into() {
            Some(&"!import")  => {
                let import = split.get(1).context("No import found!")?; // TODO: This should print the template path
                imports.push(import.to_string());
            }
            _ => continue,
        }
    }
    Ok(imports)
}

pub fn get_imports_toml(imports: Imports, import_files: Vec<PathBuf>, language: &Language, headder_file: bool) -> ImportMap {
    let mut import_map = ImportMap {
        gitignore: HashMap::new(),
        makefile: HashMap::new(),
        main: HashMap::new(),
    };

    let mut used_imports = Imports {
        gitignore: Vec::new(),
        makefile: Vec::new(),
        main: Vec::new(),
    };

    for file in import_files.into_iter().rev() {
        let import_vals = read_toml_file(&file);
        for import in imports.main.to_owned().into_iter() {

            if let Some(section) = import_vals.get(&language.lang_type.to_string()) {
                match section.get(&import) {
                    Some(val) => {
                        if let Value::String(val) = val {
                            import_map.main.insert(import.to_owned(), val.as_str().to_string());
                            used_imports.main.push(import);
                        }
                    }
                    None => (),
                };
            }
        }
        for import in imports.main.to_owned().into_iter() {

            if let Some(section) = import_vals.get("common") {
                match section.get(&import) {
                    Some(val) => {
                        if let Value::String(val) = val {
                            let new_val = replace_comments(val.as_str().to_string(), language);
                            import_map.main.insert(import.to_owned(), new_val.to_owned());
                            used_imports.main.push(import);

                        }
                    }
                    None => (),
                };
            }
        }
        for import in imports.gitignore.to_owned().into_iter() {
            if let Some(section) = import_vals.get("gitignore") {
                match section.get(&import) {
                    Some(val) => {
                        if let Value::String(val) = val {
                            import_map.gitignore.insert(import.to_owned(), val.as_str().to_string());
                            used_imports.gitignore.push(import);
                        }
                    }
                    None => (),
                };
            }
        }

        for import in imports.makefile.to_owned().into_iter() {
            if let Some(section) = import_vals.get("makefile"){
                match section.get(&import) {
                    Some(val) => {
                        if let Value::String(v) = val {
                            import_map.makefile.insert(import.to_owned(), v.as_str().to_string());
                            used_imports.makefile.push(import);
                        }
                    }
                    None => {},
                };
            }
        }
    }
    check_unused_imports(imports.main, used_imports.main, r#"main"#);
    check_unused_imports(imports.gitignore, used_imports.gitignore, r#"gitignore"#);
    check_unused_imports(imports.makefile, used_imports.makefile, r#"makefile"#);
    if !headder_file {
        import_map.main.retain(|x,_| *x != "HEADC".to_string());
    }

    import_map
}

fn check_unused_imports(imports: Vec<String>, found: Vec<String>, file: &str) {
    // Prints warning if import is not found
    let mut imports = imports.clone();
    for i in found.into_iter() {
        imports.retain(|x| i != x.to_string());
    }
    if !imports.is_empty() {
        colour::yellow!("Warning: ");
        println!("The following was imported in \"{}\" but the imports was not found.\n{:?}", file, imports) // TODO: Better err msg.
    }
}

fn create_new_config(file: &PathBuf) {
    // If no config file was found, create a default one.
    let user = get_user_by_uid(get_current_uid()).unwrap();
    let user_name = user.name().to_str().unwrap().to_string();
    std::fs::write(
        file,
        toml::to_string(&create_default_config_file(user_name)).expect("could not encode TOML value"),
    )
        .expect("Could not write to config file!");
}

fn create_default_config_file(author: String) -> Value {
    // Create default config file.
    // returns Value that is to be dumped to the toml config file
    let mut var = Map::new();
    {
        let mut value = Map::new();
        value.insert("gitignore".into(), Value::Boolean(true));
        value.insert("makefile".into(), Value::Boolean(true));
        var.insert("c".into(), Value::Table(value));
    }
    {
        let mut value = Map::new();
        value.insert("gitignore".into(), Value::Boolean(true));
        value.insert("makefile".into(), Value::Boolean(true));
        var.insert("tex".into(), Value::Table(value));
    }
    {
        let mut value = Map::new();
        value.insert("gitignore".into(), Value::Boolean(true));
        value.insert("makefile".into(), Value::Boolean(true));
        var.insert("tex".into(), Value::Table(value));
    }
    {
        let mut value = Map::new();
        value.insert("gitignore".into(), Value::Boolean(true));
        value.insert("makefile".into(), Value::Boolean(true));
        var.insert("golang".into(), Value::Table(value));
    }
    {
        let mut value = Map::new();
        value.insert("AUTHOR".into(), Value::String(author));
        var.insert("variables".into(), Value::Table(value));
    }

    {
        let mut value = Map::new();
        value.insert("gitignore".into(), Value::Boolean(true));
        value.insert("makefile".into(), Value::Boolean(true));
        value.insert("overridalbe".into(), Value::Boolean(false));
        var.insert("org".into(), Value::Table(value));
    }

    return Value::Table(var);
}

fn read_toml_file(file: &PathBuf) -> Value {
    let content = std::fs::read_to_string(&file).expect("Could not read config file");
    let parsed_toml: Value = toml::from_str(&content).unwrap();

    return parsed_toml;
}

// TODO: look for templates locally
pub fn get_gitignore_template(config_path: &PathBuf, language: &String) -> String {
    let template: String;
    let mut template_path = config_path.to_owned();
    template_path.push("templates/gitignore");
    let mut file = template_path.to_owned();
    let mut default_file = template_path.to_owned();
    file.push(language);
    default_file.push("default");
    if std::path::Path::new(&file).exists() {
        template = std::fs::read_to_string(file).expect("Could not read file");
    }
    else if std::path::Path::new(&default_file).exists() {
        template = std::fs::read_to_string(&default_file).expect("Could not read file");
    } else {
        panic!("No gitignore template file found") // TODO: Better err msg
    }
    return template;
}

pub fn get_makefile_template(config_path: &PathBuf, language: &String, override_makefile: &mut bool, makefile_flag: &mut bool, overridable: bool) -> String {
    let template: String;
    let mut makefile_path = config_path.to_owned();
    makefile_path.push("makefiles");
    makefile_path.push(language);
    makefile_path.push("Makefile");
    if std::path::Path::new(&makefile_path).exists() {
        template = std::fs::read_to_string(&makefile_path).expect("Could not read file");
    }
    else  {
        if overridable {
            colour::green!("Info: ");
            println!("No makefile found for {}, wont move makefile!", language);
        }
        template = String::from("");
        *override_makefile = true;
        *makefile_flag = false;
    }
    return template
}


fn replace_comments(import: String, language: &Language) -> String {
    let comment_begin = Regex::new(r"\$B").unwrap();
    let comment_end = Regex::new(r"\$E").unwrap();

    let is_match: bool = comment_begin.is_match(&import).unwrap();
    if !is_match {
        return import
    }

    let new_import: String;
    new_import = comment_regex(import,
                               comment_begin,
                               comment_end,
                               &language.start_multiline_comment,
                               &language.end_multiline_comment,
                               &language.non_comment_comment);

    return new_import
}

fn comment_regex(val: String, comment_begin: Regex, comment_end: Regex, start_comment: &str, end_comment: &str, non_comment_comment: &str) -> String {
    let mut new_val = String::new();
    let lines: Vec<&str> = val.split("\n").collect();
    for line in lines {
        if comment_begin.is_match(line).unwrap() {
            new_val = new_val + &line.replace("$B", start_comment) + "\n"

        } else if comment_end.is_match(line).unwrap() {
            new_val = new_val + &line.replace("$E", end_comment) + "\n"
        }
        else if line != "" {
            new_val = new_val + non_comment_comment + line + "\n";
        }
    }

    return new_val;
}
